package com.dataofthestorm.web;

import org.springframework.boot.builder.SpringApplicationBuilder;

public class LocalServer extends App {

    public static void main(String[] args) {
        new LocalServer().configure(new SpringApplicationBuilder()).run(args);
    }
}
