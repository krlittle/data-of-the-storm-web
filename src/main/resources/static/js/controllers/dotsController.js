dotsApplication.controller('DotsController', ['$scope', '$http', 'DotsService', function($scope, $http, DotsService) {

    $scope.dotsResponse = '';

    $scope.search = function() {
        DotsService.search().then(function(response) {
            $scope.dotsResponse = response.data.body;
            console.log($scope.dotsResponse);
        });
    }

}]);