var dotsApplication = angular.module('dotsApplication', ['ngRoute']);

dotsApplication.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'partials/home.html',
                controller: 'DotsController'
            }).otherwise({
                redirectTo: '/'
            });
    }
]);