package com.dataofthestorm.web.services;

import com.dataofthestorm.web.helpers.HttpHeadersHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class DataOfTheStormService {

    @Autowired
    private HttpHeadersHelper httpHeadersHelper;

    @Autowired
    private RestTemplate restTemplate;

    public Object getReplays(String url) {
        return restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(httpHeadersHelper.httpHeaders()), Object.class);
    }
}
