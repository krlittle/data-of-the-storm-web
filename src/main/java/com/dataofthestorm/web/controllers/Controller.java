package com.dataofthestorm.web.controllers;

import com.dataofthestorm.web.services.DataOfTheStormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @Value("${dataofthestorm.services.url}")
    private String url;

    @Autowired
    private DataOfTheStormService service;

    @GetMapping("/replays")
    public ResponseEntity<?> getReplays() {
        return ResponseEntity.ok(service.getReplays(url + "/replays"));
    }
}
